# Chalo Starter Project

## What is this?

This is a basic shell project you can use to get started with Chalo. It starts up the program and has basic functionality that you can use to expand into your own game.

## How do I use this?

To get started, **clone** this repository, and **clone** the Chalo Engine repostory (https://bitbucket.org/moosaderllc/chalo-engine/src) into the path: TODO
